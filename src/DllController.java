import com.sun.jna.Library;
import com.sun.jna.Native;

public class DllController {

	public interface autoHotKeyDll extends Library {
		public void ahkExec(char[] s);
		public void ahktextdll(char[] s);
	}

	public static void main(String args[]) throws InterruptedException {

		System.out.println("running in " + System.getProperty("sun.arch.data.model"));

		System.out.println("Loading dll");
		autoHotKeyDll lib = (autoHotKeyDll) Native.loadLibrary("AutoHotkey", autoHotKeyDll.class);

		System.out.println("initialisation");
		lib.ahktextdll("#Persistent".toCharArray());
//		
//		lib.ahkExec("#Persistent".toCharArray());
		Thread.sleep(4000);
		System.out.println("displaying cbBox");
//		lib.ahkExec("Send,{Numpad2 Down}{Numpad2 Up}".toCharArray());
		lib.ahkExec("Send,{Enter Down}{Enter Up}".toCharArray());
	}
	
}