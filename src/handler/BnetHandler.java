package handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;

import exception.PeopleNotFoundException;
import exception.PlayerNotFoundException;
import exception.StringNotFoundException;
import mainSys.Capturer;
import model.Ability;
import model.Coords;
import model.Indicator;
import model.People;
import model.Player;
import utils.ByteHexTool;
import utils.StringHelper;

public class BnetHandler implements PcapPacketHandler {

	int counter = 0;
	int count = 0;

	public void nextPacket(PcapPacket packet, Object user) {
		try {

			Payload payload = new Payload();
			if (packet.hasHeader(payload)) {
				// byte[] allBytes = payload.getPayload();
				//
				JBuffer jBuffer = packet.getHeader(payload);
				byte[] allBytes = jBuffer.getByteArray(0, jBuffer.size());
				String allByteString = ByteHexTool.bytesToHex(allBytes);
				String inCommingString = "";

				String containActionString = "";

				try {
					inCommingString = StringHelper.findAndGetFromHere(allByteString, "F70C".toUpperCase());
					containActionString = inCommingString;// inCommingString;

				} catch (StringNotFoundException e1) {
					// TODO Auto-generated catch block
					// e1.printStackTrace();
					// containActionString = allByteString;// i
				}

				try {
					// System.out.println("inCommingString(Boom): " +
					// inCommingString);
					if (inCommingString.indexOf("400037323049fff".toUpperCase()) != -1) {// boom

						int stringIndex = inCommingString.indexOf("400037323049fff".toUpperCase());
						System.out.println(stringIndex);
						int playerNoIndex = stringIndex - 8;
						String playerNoString = inCommingString.substring(playerNoIndex, playerNoIndex + 2);
						// JOptionPane.showMessageDialog(null, playerNoString);
						int playerNo = Integer.parseInt(playerNoString, 16);
						Capturer.getPlayer(playerNo).buyBoom();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				// JOptionPane.showMessageDialog(null, "ninja");
				try {
					// System.out.println("inCommingString(ninja): " +
					// inCommingString);
					if (inCommingString.indexOf("40007774646efffff".toUpperCase()) != -1) {// ninja
						// JOptionPane.showMessageDialog(null, "ninja");
						new Thread(new Runnable() {
							// The wrapper thread is unnecessary, unless it
							// blocks on the
							// Clip finishing; see comments.

							private final int BUFFER_SIZE = 128000;
							private File soundFile;
							private AudioInputStream audioStream;
							private AudioFormat audioFormat;
							private SourceDataLine sourceLine;

							public void run() {
								// String strFilename = "bomb.wav";
								String strFilename = "ninja.wav";
								try {
									soundFile = new File(strFilename);
								} catch (Exception e) {
									e.printStackTrace();
									System.exit(1);
								}

								try {
									audioStream = AudioSystem.getAudioInputStream(soundFile);
								} catch (Exception e) {
									e.printStackTrace();
									System.exit(1);
								}

								audioFormat = audioStream.getFormat();

								DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
								try {
									sourceLine = (SourceDataLine) AudioSystem.getLine(info);
									sourceLine.open(audioFormat);
								} catch (LineUnavailableException e) {
									e.printStackTrace();
									System.exit(1);
								} catch (Exception e) {
									e.printStackTrace();
									System.exit(1);
								}

								sourceLine.start();

								int nBytesRead = 0;
								byte[] abData = new byte[BUFFER_SIZE];
								while (nBytesRead != -1) {
									try {
										nBytesRead = audioStream.read(abData, 0, abData.length);
									} catch (IOException e) {
										e.printStackTrace();
									}
									if (nBytesRead >= 0) {
										@SuppressWarnings("unused")
										int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
									}
								}

								sourceLine.drain();
								sourceLine.close();
							}

						}).start();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				String compareString = containActionString;
				if (containActionString.indexOf("F7270900") != -1) {
					compareString = containActionString.substring(0, containActionString.indexOf("F7270900"));

				}

				try {
					Capturer.indicators.add(Indicator.getIndicatorFromPacket(compareString));
				} catch (StringNotFoundException e1) {
					// TODO Auto-generated catch block
					// e1.printStackTrace();
					// e.printStackTrace();
				}
				// 俴
				// 000003000d00ffffffffffffffffdcfb0cc4475b9644ffffffffffffffff
				getPeopleLocation(compareString);

				// int
				// playerNo =
				// Integer.parseInt(compareString.substring(playerNoIndex,
				// playerNoIndex + 2), 16);

				// System.out.println(compareString);
				// f70c1a002f0060080a0f001044003931304fffffffffffffffff
				if (compareString.length() >= 23) {
					//
					// System.out.println(compareString);
					// System.out.println("=======create people code");
					// ;
					// System.out.println(compareString.substring(18,
					// 24).toUpperCase());
					try {
						// ArrayList <Integer> indicatorIndexs=new
						// ArrayList();
						// Pattern p = Pattern.compile("00010"); // insert
						// your pattern here
						// Matcher m = p.matcher(documentText);
						// while (m.find()) {
						// positions.add(m.start());
						// }
						if (compareString.substring(18, 24).toUpperCase().equals("0f0010".toUpperCase())) {// 0F0010
							// 1FB310
							// JOptionPane.showMessageDialog(null,compareString.substring(24).toUpperCase());
							// count++;
							// System.out.println(" count: " + count);
							try {
								// for (People p : peoples) {

								People tempPeople;
								try {
									tempPeople = People.findPeople(compareString.substring(24).toUpperCase());
									// JOptionPane.showMessageDialog(null,
									// tempPeople.getName());

									System.out.println(compareString.substring(19, 25).toUpperCase());
									Player player = People.findPlayerNoGenPeople(compareString,
											tempPeople.getPeopleCode());
									// JOptionPane.showMessageDialog(null,player.getID());
									System.out.println(compareString);

									// Player
									// player=Player.getPlayer(playerNo);
									player.setPeople(tempPeople);
								} catch (PeopleNotFoundException e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								} catch (PlayerNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								String remainingString = StringHelper.findAndGetFromHere(compareString.toUpperCase(),
										"0f0010".toUpperCase());
								// JOptionPane.showMessageDialog(null,
								// "remaining" + remainingString);
								remainingString = remainingString.substring(28);
								ArrayList<Integer> indicatorsIndexs = new ArrayList();
								for (int i = 0; i < remainingString.length() - "0f0010".length(); i++) {
									// JOptionPane.showMessageDialog(null,
									// remainingString.substring(i,
									// i+"0f0010".length()).toUpperCase()+"
									// vs "+"0f0010".toUpperCase());
									if (remainingString.substring(i, i + "0f0010".length()).toUpperCase()
											.equals("0f0010".toUpperCase())) {
										indicatorsIndexs.add(i);
										// JOptionPane.showMessageDialog(null,
										// "indicatorsIndexs.size:"+indicatorsIndexs.size());
									}

								}
								// JOptionPane.showMessageDialog(null,
								// "indicatorsIndexs.finalSize:" +
								// indicatorsIndexs.size());
								for (int indicatorsIndex : indicatorsIndexs) {
									String playerNoString = remainingString.substring(indicatorsIndex - 2,
											indicatorsIndex);
									String peoplePattern = remainingString.substring(
											indicatorsIndex + "0F0010".length(),
											indicatorsIndex + "0F0010".length() + 13);
									// JOptionPane.showMessageDialog(null,
									// playerNoString+" "+peoplePattern);

									try {
										tempPeople = People.findPeople(peoplePattern.toUpperCase());
										// JOptionPane.showMessageDialog(null,
										// tempPeople.getName());

										// System.out.println(compareString.substring(19,
										// 25).toUpperCase());
										Player player = Player.getPlayerWithIDString(playerNoString);
										// Player player =
										// People.findPlayerNoGenPeople(compareString,
										// tempPeople.getPeopleCode());
										// JOptionPane.showMessageDialog(null,player.getID());
										System.out.println(compareString);

										// Player
										// player=Player.getPlayer(playerNo);
										player.setPeople(tempPeople);
									} catch (PeopleNotFoundException e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									} catch (PlayerNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}

								// }
							} catch (Exception e) {
								e.printStackTrace();
							}

							// String remainingString=

						}

					} catch (IndexOutOfBoundsException e) {
						// Do Something...
						e.printStackTrace();
					}
				}

				for (People people : Capturer.peoples) {

					if (people.haveAbilityWithCode(compareString)) {

						Ability ability = people.getAbilityNameWithCode(compareString);

						ability.resetTime();

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getPeopleLocation(String compareString) {
		if (compareString.contains("000003000d00".toUpperCase())) {
			String walkString = compareString.substring(compareString.indexOf("000003000d00".toUpperCase()));

			String xString = walkString.substring(28, 36);
			String yString = walkString.substring(36, 44);

			System.out.println("XString:" + xString);
			System.out.println("YString:" + yString);

			String rightXString = ByteHexTool.hexStringLittleEndianOrder(xString);
			String rightYString = ByteHexTool.hexStringLittleEndianOrder(yString);
			System.out.println("rightXString:" + rightXString);
			System.out.println("rightYSting:" + rightYString);

			// long x=Long.parseLong(rightXString, 16);
			// long y=Long.parseLong(rightYString, 16);
			float x = ByteHexTool.hexStringToFloat(rightXString);
			float y = ByteHexTool.hexStringToFloat(rightYString);

			System.out.println(compareString);
			int playerNo = -1;
			int walkdDataIndex = compareString.indexOf("000003000d00".toUpperCase());
			int playerNoIndex = walkdDataIndex - 8;
			playerNo = Integer.parseInt(compareString.substring(playerNoIndex, playerNoIndex + 2), 16);
			System.out.println("playerNo" + playerNo);
			try {

				Capturer.getPlayer(playerNo).humanNewLocation(new Coords(x, y));
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			// coords.add(new Coords(x,y));
			// System.out.println("Walk"+walkString);
			System.out.println(x + "\t" + y);
			// GUIMain.updateMinMap();
		}
	}

}
