package listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import mainSys.GUIMain;

public class UpdateDataListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {

		GUIMain.updateData();

	}
}
