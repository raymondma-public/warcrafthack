package mainSys;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import exception.PlayerNotFoundException;
import listener.UpdateDataListener;
import listener.UpdateUIListener;
import model.Config;
import model.Coords;
import model.Indicator;
import model.People;
import model.Player;
import utils.CoordFinder;
import utils.ImageUtil;
import utils.Loader;
import utils.TimerTime;

public class GUIMain {

	static JTextArea textAreaLeft = new JTextArea(30, 30);
	static JTextArea textAreaMid = new JTextArea(30, 30);
	static JTextArea textAreaRight = new JTextArea(30, 30);
	static JTextArea textAreaTop = new JTextArea(1, 1);
	static JPanel minMap = new JPanel();

	public static void updateGUI() {

		// show
		String showStringLeft = "";
		String showStringMid = "";
		String showStringRight = "";
		for (People people : Capturer.peoples) {
			switch (people.getTeam()) {
			case People.LEFT_TEAM:
				showStringLeft += people.toString() + "\n";
				break;
			case People.MID_TEAM:
				showStringMid += people.toString() + "\n";
				break;
			case People.RIGHT_TEAM:
				showStringRight += people.toString() + "\n";
				break;
			}
		}
		textAreaLeft.setText(showStringLeft);
		textAreaMid.setText(showStringMid);
		textAreaRight.setText(showStringRight);

		String waterInfo = "";
		for (Player player : Capturer.players) {
			waterInfo += player.getID() + ":" + player.getWaterNo() + "\t";

		}
		textAreaTop.setText(waterInfo);
		// JOptionPane.showMessageDialog(null, showString);
	}

	private static void allAbilityMinusOne() {
		// TODO Auto-generated method stub
		for (People people : Capturer.peoples) {
			people.allAbilityMinusOne();
		}
	}

	private static TimerTime updateUITimer = null;
	private static TimerTime updateDataTimer = null;

	private static void createAndShowGUI() {

		// Create and set up the window.
		JFrame frame = new JFrame("HelloWorldSwing") {
			// public void paint(Graphics g) {
			// super.paint(g);
			// g.drawString("Hello", 200, 50);
			// for (Coords c : Capturer.coords) {
			// Graphics2D g2d = (Graphics2D)g;
			// g2d.drawOval(c.getX(), c.getY(), 100, 100);
			// }
			//
			// }
		};
//		frame.setSize(500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		minMap.setSize(300, 300);
		// Add the ubiquitous "Hello World" label.
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(textAreaLeft, BorderLayout.WEST);
		frame.getContentPane().add(textAreaMid, BorderLayout.CENTER);
		frame.getContentPane().add(textAreaRight, BorderLayout.EAST);
		frame.getContentPane().add(textAreaTop, BorderLayout.NORTH);
		frame.getContentPane().add(minMap, BorderLayout.SOUTH);

		textAreaLeft.setBorder(BorderFactory.createLineBorder(Color.black));
		textAreaMid.setBorder(BorderFactory.createLineBorder(Color.black));
		textAreaRight.setBorder(BorderFactory.createLineBorder(Color.black));
		textAreaTop.setBorder(BorderFactory.createLineBorder(Color.black));

		updateUITimer = new TimerTime(250, new UpdateUIListener());
		updateDataTimer = new TimerTime(1000, new UpdateDataListener());

		// Display the window.
		// frame.locate(1600, 0);
		// frame.getComponentAt(1600, 0);
		frame.setBounds(Config.getInstance().getX(), Config.getInstance().getY(), Config.getInstance().getWidth(), Config.getInstance().getHeight());
		frame.pack();
		frame.setVisible(true);
	}

	static JFrame minMapFrame = null;

	private static BufferedImage image = null;

	private static JPanel inputJPanel = new JPanel();

	static BufferedImage capture = null;

	private static void createAndShowMinMap() {

		setupInputPanel();
		try {
			image = ImageIO.read(new File("smallMH.jpg"));
			// image = ImageIO.read(new File("mh.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Create and set up the window.

		new Thread() {

			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while (true) {
					try {
						capture = new Robot().createScreenCapture(screenRect);
						capture = ImageUtil.cropImage(capture, 17, 682, 282, 208);
					} catch (AWTException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

		}.start();

		minMapFrame = new JFrame("MinMap") {
			int topOffset = 80;

			public void paint(Graphics g) {
				super.paint(g);

				g.drawImage(capture, 0, topOffset, minMapFrame.getWidth(), minMapFrame.getHeight() - topOffset - 30,
						null);

				// g.drawString("MinMap", 0, 0);
				// int minX=-960;
				// int maxX=3008;
				// int minY=-2496;
				// int maxY=1472;
				int minX = -8130;
				int maxX = 8130;
				int minY = -8450;
				int maxY = 8315;
				// System.out.println("Capturer.players.size()"+Capturer.players.size());;
				for (Player player : Capturer.players) {
					drawOnePeople(g, minX, maxX, minY, maxY, player);
				}

				ArrayList<Indicator> destroyList = new ArrayList();
				g.setColor(new Color(1f,1f,1f,0.7f));
				
				// System.out.println("Capturer.indicators.size()
				// "+Capturer.indicators.size());
				
				for (Indicator indicator : Capturer.indicators) {
					float finalX = indicator.getX() + Math.abs(minX);
					float finalY = (maxY + Math.abs(minY)) - (indicator.getY() + Math.abs(minY));

					Graphics2D g2d = (Graphics2D) g;

					float xOnCanvas = (float) CoordFinder.findXfromValue(finalX, maxX + Math.abs(minX),
							minMapFrame.getWidth());
					float yOnCanvas = (float) CoordFinder.findYfromValue(finalY, maxY + Math.abs(minY),
							minMapFrame.getHeight() - topOffset);
					g.fillOval((int) xOnCanvas- indicator.getSize()/2, (int) yOnCanvas + topOffset- indicator.getSize()/2, indicator.getSize(), indicator.getSize());
					indicator.refresh();
					if (indicator.shouldDestroy()) {
						// destroy
						destroyList.add(indicator);
					}
				}

				Capturer.indicators.removeAll(destroyList);

				destroyList.clear();
			}

			
			private float[] tempLastXArr=new float[10];
			private float[] tempLastYArr=new float[10];
			
			private void drawOnePeople(Graphics g, int minX, int maxX, int minY, int maxY, Player player) {
				int count = 0;
				int playerIndex = player.getID();

				int r = 255, gr = 0, b = 0;
				float satuation = 50;
//				synchronized (this) {
					for (Coords c : player.getWholeHumanLocationQueue()) {

						float finalX = c.getX() + Math.abs(minX);
						float finalY = (maxY + Math.abs(minY)) - (c.getY() + Math.abs(minY));

						Graphics2D g2d = (Graphics2D) g;

						float xOnCanvas = (float) CoordFinder.findXfromValue(finalX, maxX + Math.abs(minX),
								minMapFrame.getWidth());
						float yOnCanvas = (float) CoordFinder.findYfromValue(finalY, maxY + Math.abs(minY),
								minMapFrame.getHeight() - topOffset);
								// System.out.println("\t"+xOnCanvas+ " "+
								// yOnCanvas );

						// System.out.println(minMapFrame.getWidth()+ " "+
						// minMapFrame.getHeight() );
						float hue = 0;

						switch (playerIndex) {
						// case 0:
						//
						// break;
						case 1:
							hue = 360;
							break;
						case 2:
							hue = 240;
							break;
						case 3:
							hue = 165;
							break;
						case 4:
							hue = 285;
							break;
						case 5:
							hue = 60;
							break;
						case 6:
							hue = 45;
							break;
						case 7:
							hue = 120;
							break;
						case 8:
							hue = 300;
							break;
						// case 9:
						// hue=
						// break;
						case 10:
							hue = 195;
							break;
						}
						// System.out.println(teamMode+" "+player.getID()+" ");
						// for(int sameTeamNo:teamList){
						// System.out.print(sameTeamNo+" ");
						// }
						// System.out.println();

						if (!teamMode) {// not team mode
							if (playerIndex == 9) {
								g.setColor(Color.getHSBColor(0, 0, satuation / 100));
							} else {
								g.setColor(Color.getHSBColor(hue / 360, satuation / 100, 100 / 100));
							}
						} else {// team mode
							if (teamList.contains(playerIndex)) {// same team
								hue = 240;
								g.setColor(Color.getHSBColor(hue / 360, satuation / 100, 100 / 100));
							} else {// not same team
								hue = 360;
								g.setColor(Color.getHSBColor(hue / 360, satuation / 100, 100 / 100));
							}
						}

						g2d.setFont(new Font("TimesRoman", Font.PLAIN, 30));

						if (count >= Config.NO_HUMANLOCATION_STORED - 1
								|| count >= player.getWholeHumanLocationQueue().size() - 1) {
							// g2d.fillOval((int) xOnCanvas, (int) yOnCanvas,
							// 25, 25);
							if ((teamMode && !teamList.contains(playerIndex)) || !teamMode) {
								tempLastXArr[playerIndex-1]=xOnCanvas;
								tempLastYArr[playerIndex-1]=yOnCanvas;
								Player tempPlayer = Player.getPlayerWithID(playerIndex);
								tempPlayer.checkBombInvalid();
								try {
									
									
									g2d.drawString(playerIndex+"." + tempPlayer.getPeople().getName()+tempPlayer.getBoomString(), (int) xOnCanvas,
											(int) yOnCanvas + topOffset);
									
//									JOptionPane.showMessageDialog(null, "Set"+playerIndex);
//								} catch (PlayerNotFoundException e) {
//									// TODO Auto-generated catch block
//									g2d.drawString("" + playerIndex, (int) xOnCanvas, (int) yOnCanvas + topOffset);
								} catch (NullPointerException e) {
									g2d.drawString("" + playerIndex+tempPlayer.getBoomString(), (int) xOnCanvas, (int) yOnCanvas + topOffset);
								}

							} else {
								tempLastXArr[playerIndex-1]=xOnCanvas;
								tempLastYArr[playerIndex-1]=yOnCanvas;
								// g2d.drawString("" + playerIndex, (int)
								// xOnCanvas, (int) yOnCanvas + topOffset);
								Player tempPlayer = Player.getPlayerWithID(playerIndex);
								tempPlayer.checkBombInvalid();
								try {
								
									System.out.println(tempPlayer.getID());
									g2d.drawString(playerIndex+"." +  tempPlayer.getPeople().getName()+tempPlayer.getBoomString(), (int) xOnCanvas,
											(int) yOnCanvas + topOffset);
//								} catch (PlayerNotFoundException e) {
//									// TODO Auto-generated catch block
//									g2d.drawString("" + playerIndex, (int) xOnCanvas, (int) yOnCanvas + topOffset);
								} catch (NullPointerException e) {
									g2d.drawString("" + playerIndex+tempPlayer.getBoomString(), (int) xOnCanvas, (int) yOnCanvas + topOffset);
								}

							}
						} else {
							if ((teamMode && !teamList.contains(playerIndex)) || !teamMode) {
								g2d.fillOval((int) xOnCanvas, (int) yOnCanvas + topOffset, 10, 10);
//								g2d.drawLine((int)tempLastXArr[playerIndex-1], (int)tempLastYArr[playerIndex-1] + topOffset,(int) xOnCanvas,(int) yOnCanvas + topOffset);
							} else {
								g2d.fillOval((int) xOnCanvas, (int) yOnCanvas + topOffset, 10, 10);
								
							}
							
						}
						g2d.drawLine((int)tempLastXArr[playerIndex-1], (int)tempLastYArr[playerIndex-1] + topOffset,(int) xOnCanvas,(int) yOnCanvas + topOffset);
						// count+=.1;
						// if(count>1){
						// count=20;
						// }

						satuation += 10;
						if (count >= Config.NO_HUMANLOCATION_STORED) {
							satuation = 0;
						}
						count++;
					}
				}
//			}
		};

//		minMapFrame.setSize(500, 500);

		minMapFrame.getContentPane().setLayout(new BorderLayout());
		minMapFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		minMapFrame.setLayout(new BorderLayout());
		inputJPanel.setVisible(true);
		minMapFrame.add(inputJPanel, BorderLayout.NORTH);

		updateUITimer = new TimerTime(250, new UpdateUIListener());
		// updateDataTimer = new TimerTime(1000, new UpdateDataListener());

		// Display the window.
		minMapFrame.setBounds(Config.getInstance().getX(), Config.getInstance().getY()+300, Config.getInstance().getWidth(), Config.getInstance().getHeight());
		minMapFrame.setPreferredSize(new Dimension(1000, 560));
		minMapFrame.pack();
		minMapFrame.setVisible(true);
	}

	private static JLabel label = new JLabel("同隊");
	private static JTextField tf1 = new JTextField(5);
	private static JTextField tf2 = new JTextField(5);
	private static JTextField tf3 = new JTextField(5);
	private static JTextField tf4 = new JTextField(5);
	private static JTextField tf5 = new JTextField(5);
	private static JButton inputBtn = new JButton("Input//Toggle");
	private static JButton resetBtn = new JButton("reset");

	private static void setupInputPanel() {
		// TODO Auto-generated method stub
		inputJPanel.setLayout(new FlowLayout());
		inputJPanel.add(label);
		inputJPanel.add(tf1);
		inputJPanel.add(tf2);
		inputJPanel.add(tf3);
		inputJPanel.add(tf4);
		inputJPanel.add(tf5);
		inputJPanel.add(inputBtn);
		inputJPanel.add(resetBtn);
		inputBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				toggleTeamColor();
			}
		});

		resetBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				tf1.setText("");
				tf2.setText("");
				tf3.setText("");
				tf4.setText("");
				tf5.setText("");
				teamList.clear();
				for (Player player : Capturer.players) {
					player.resetFirst();
				}
			}
		});

	}

	private static boolean teamMode = true;
	private static ArrayList<Integer> teamList = new ArrayList();

	public static void addTeamate(int number) {
		teamList.add(number);
	}

	protected static void toggleTeamColor() {
		// TODO Auto-generated method stub
		teamList.clear();
		try {
			teamList.add(Integer.parseInt(tf1.getText()));
		} catch (NumberFormatException e) {

		}

		try {
			teamList.add(Integer.parseInt(tf2.getText()));
		} catch (NumberFormatException e) {

		}
		try {
			teamList.add(Integer.parseInt(tf3.getText()));
		} catch (NumberFormatException e) {

		}
		try {
			teamList.add(Integer.parseInt(tf4.getText()));
		} catch (NumberFormatException e) {

		}
		try {
			teamList.add(Integer.parseInt(tf5.getText()));
		} catch (NumberFormatException e) {

		}
		toggleTeamMode();
	}

	private static void toggleTeamMode() {
		// TODO Auto-generated method stub
		if (teamMode) {
			teamMode = false;
		} else {
			teamMode = true;
		}
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		Config.getInstance().readConfigFromFile();

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				
				// ClassicPcapExample.main(null);
			}
		});
		
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowMinMap();
				Loader loader=new Loader();
				loader.loadfromFile("peopleCode2.TXT");
				// ClassicPcapExample.main(null);
			}

		});
		new Runnable() {
			public void run() {

				Capturer.main(null);
			}
		}.run();

		
		// HttpServer server = HttpServer.create(new InetSocketAddress(8000),
		// 0);
		// server.createContext("/test", new MyHandler());
		// server.setExecutor(null); // creates a default executor
		// server.start();
	}

	public static void updateData() {
		// TODO Auto-generated method stub
		// data--
		allAbilityMinusOne();
	}

	public static void updateMinMap() {
		// TODO Auto-generated method stub
		minMapFrame.repaint();
	}
}
