package model;

public class Ability {
	private String name;
	private String code;
	private int loadingTime;
	private final int oLoadingTime;

	public Ability(String name, String code, int loadingTime) {
		this.name = name;
		this.code = code;
		this.loadingTime = 0;
		this.oLoadingTime = loadingTime;
	}

	public boolean containCode(String compareString) {
		// TODO Auto-generated method stub
		return compareString.contains(code) || compareString.contains(code.toUpperCase());
	}

	public String getName() {
		return this.name;
	}

	public String toString() {

		String tempString = "";
		if (!(this.loadingTime <= 0)) {
			tempString += "\t";
		}
		tempString+= this.name + " time: " + loadingTime;
		return tempString;
	}

	public void minusOne() {
		// TODO Auto-generated method stub
		if (loadingTime > 0) {
			loadingTime--;
		}
	}

	public void resetTime() {
		// TODO Auto-generated method stub
		this.loadingTime = this.oLoadingTime;
	}
}
