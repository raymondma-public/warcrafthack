package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Config {
	public static final int NO_HUMANLOCATION_STORED=5;
	
	private int x;
	private int y;
	private int width;
	private int height;
	private int lan;
	private static Config instance=new Config();
	
	private Config(){
		
	}
	public static Config getInstance(){
		return instance;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLan() {
		return lan;
	}

	public void setLan(int lan) {
		this.lan = lan;
	}
	
	public void readConfigFromFile(){
		try {
			Scanner scanner=new Scanner(new File("config.ini"));
			while(scanner.hasNextLine()){
				String wholeLine=scanner.nextLine();
				String[]wholeLineSplit=wholeLine.split("=");
				String key=wholeLineSplit[0];
				String valueString=wholeLineSplit[1];
				int value=Integer.parseInt(valueString);
				if(key.equalsIgnoreCase("x")){
					this.setX(value);
				}
				if(key.equalsIgnoreCase("y")){
					this.setY(value);
				}
				if(key.equalsIgnoreCase("width")){
					this.setWidth(value);
				}
				if(key.equalsIgnoreCase("height")){
					this.setHeight(value);
				}
				if(key.equalsIgnoreCase("lan")){
					this.setLan(value);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Config c=Config.getInstance();
		c.readConfigFromFile();
				
		System.out.println("x:"+c.getX());
		System.out.println("y:"+c.getY());
		System.out.println("width:"+c.getWidth());
		System.out.println("height:"+c.getHeight());
		System.out.println("lan:"+c.getLan());
	}

}
