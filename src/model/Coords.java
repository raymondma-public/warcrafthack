package model;

public class Coords {
	private float x;
	private float y;
	public Coords(float x,float y){
		this.x=x;
		this.y=y;
	}
	
	public float getX(){
		return x;
		
	}
	public float getY(){
		return y;
	}
}
