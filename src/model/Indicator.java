package model;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import exception.StringNotFoundException;
import utils.ByteHexTool;

public class Indicator {
	private Coords coordination;
	private int occurCount = 30;
	private int size = 10;

	public Indicator(float x, float y) {
		coordination = new Coords(x, y);
//		if()
		playSound();
	}

	public int getSize() {
		return size;
	}

	public float getX() {
		return coordination.getX();
	}

	public float getY() {
		return coordination.getY();
	}

	public boolean shouldDestroy() {
		if (occurCount <= 0) {
			return true;
		} else {
			return false;
		}

	}

	private void minusCount() {
		occurCount--;

	}

	public void refresh() {
		minusCount();
//		minusSize();
		increaseSize();
	}
	private void increaseSize() {
		size+=2;
	}
	private void minusSize() {
		size--;
	}

	public static Indicator getIndicatorFromPacket(String wholeString) throws StringNotFoundException {
		wholeString = wholeString.toUpperCase();
		String findString = "0000a040".toUpperCase();

		String resultString = "";
		if ((wholeString.indexOf(findString) != -1)) {
			int refIndex = wholeString.indexOf(findString);
			String yInverseString = wholeString.substring(refIndex - 8, refIndex);
			String xInverseString = wholeString.substring(refIndex - 16, refIndex - 8);

			String correctXString = ByteHexTool.hexStringLittleEndianOrder(xInverseString);
			String correctYString = ByteHexTool.hexStringLittleEndianOrder(yInverseString);

			System.out.println("Indicator x:" + correctXString);
			System.out.println("Indicator y:" + correctYString);

			float x = ByteHexTool.hexStringToFloat(correctXString);
			float y = ByteHexTool.hexStringToFloat(correctYString);
			return new Indicator(x, y);
			// resultString = wholeString.substring(());
		} else {
			throw new StringNotFoundException();
		}

	}

	public static synchronized void playSound() {

		new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.

			private final int BUFFER_SIZE = 128000;
			private File soundFile;
			private AudioInputStream audioStream;
			private AudioFormat audioFormat;
			private SourceDataLine sourceLine;

			public void run() {
				String strFilename = "ding.wav";
				try {
					soundFile = new File(strFilename);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}

				try {
					audioStream = AudioSystem.getAudioInputStream(soundFile);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}

				audioFormat = audioStream.getFormat();

				DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
				try {
					sourceLine = (SourceDataLine) AudioSystem.getLine(info);
					sourceLine.open(audioFormat);
				} catch (LineUnavailableException e) {
					e.printStackTrace();
					System.exit(1);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}

				sourceLine.start();

				int nBytesRead = 0;
				byte[] abData = new byte[BUFFER_SIZE];
				while (nBytesRead != -1) {
					try {
						nBytesRead = audioStream.read(abData, 0, abData.length);
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (nBytesRead >= 0) {
						@SuppressWarnings("unused")
						int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
					}
				}

				sourceLine.drain();
				sourceLine.close();
			}

		}).start();
	}
}
