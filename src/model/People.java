package model;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import exception.PeopleNotFoundException;
import exception.PlayerNotFoundException;
import mainSys.Capturer;

public class People {
	private String name;
	private ArrayList<Ability> abilities;
	private int team;
	public String peopleCode="";
	
	public static final int LEFT_TEAM=-1;
	public static final int MID_TEAM=0;
	public static final int RIGHT_TEAM=1;
	
	
	public People(String name,ArrayList<Ability> abilities,int team,String peopleCode){
		this.name=name;
		this.abilities=abilities;
		this.team=team;
		this.peopleCode=peopleCode;
			
	}

	
	public String getName(){
		return this.name;
	}
	public String getPeopleCode(){
		return this.peopleCode;
	}
	
	public int getTeam(){
		return team;
	}
	public boolean haveAbilityWithCode(String compareString) {
		// TODO Auto-generated method stub
		for(Ability ability:abilities){
			if(ability.containCode(compareString)){
				return true;
			}else{
				
			}
		}
		return false;
	}
	public Ability getAbilityNameWithCode(String compareString){
		for(Ability ability:abilities){
			if(ability.containCode(compareString)){
				return ability;
			}else{
				
			}
		}
		return null;
	}
	
	public String toString(){
		String tempString="";
		for(Ability ability:abilities){
			tempString+="\n"+ability.toString();
		}
		tempString+="\n";
		return this.name+tempString;
	}
	public void allAbilityMinusOne() {
		// TODO Auto-generated method stub
		for(Ability ability:abilities){
			ability.minusOne();
		}
	}

	public static People findPeople(String compareString) throws PeopleNotFoundException {
		int tryCount=1;
		for(People p:Capturer.peoples){
//			System.out.println("===========compare "+tryCount);
//			System.out.println(compareString);
//			System.out.println(p.getPeopleCode());
			
			
			if(compareString.substring(0,12).toUpperCase().equals(p.getPeopleCode().substring(0,12).toUpperCase())){
//				if(compareString.toUpperCase().contains(p.getPeopleCode().substring(0,12).toUpperCase())){
//					JOptionPane.showMessageDialog(null, compareString);
				return p;
			}
			tryCount++;
		}
	
		throw new PeopleNotFoundException();
		
	}

	public static Player findPlayerNoGenPeople(String compareString,String peopleCode) throws PlayerNotFoundException {
	
		int index=compareString.toUpperCase().indexOf(peopleCode.toUpperCase());
		int playerNumberIndex=index-8;
		String playerNoString=compareString.substring(playerNumberIndex,playerNumberIndex+2);
//		JOptionPane.showMessageDialog(null,
//	    ""+playerNoString);
		int playerNo=0;
		if(playerNoString.equals("0A")){
			playerNo=10;
		}else{
			playerNo=Integer.parseInt(playerNoString);
		}
		
		for(Player p:Capturer.players){
			if(p.getID()==playerNo){
				return p;
			}
		}
		
		throw new PlayerNotFoundException();
	}

//	public static void findAndSetToPlayer(String playerNoString, String compareString) {
//		// TODO Auto-generated method stub
//		People tempPeople;
//		try {
//			tempPeople = People.findPeople(compareString);
//	
//		 JOptionPane.showMessageDialog(null,tempPeople.getName());
//		 Player player = People.findPlayerNoGenPeople(compareString,
//					tempPeople.getPeopleCode());
////			 JOptionPane.showMessageDialog(null,player.getID());
//			System.out.println(compareString);
//
//			// Player
//			// player=Player.getPlayer(playerNo);
//			player.setPeople(tempPeople);
//		} catch (PeopleNotFoundException e) {
//			// TODO Auto-generated catch block
////			e.printStackTrace();
//		} catch (PlayerNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}



}
