package model;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import exception.PlayerNotFoundException;
import mainSys.Capturer;
import mainSys.GUIMain;

public class Player {
	private long water;
	private int ID;
	private boolean haveBoom;
	private Queue<Coords> humanCoords = new LinkedList();
	private boolean firstLocation = true;
	private Date buyTime=new Date();
	private People people;

	public boolean haveBoom(){
		return haveBoom;
	}
	public void setPeople(People people) {
		this.people = people;
	}

	public People getPeople() {
		return people;
	}

	public Player(int ID) {
		this.ID = ID;
		System.out.println("ID " + ID);
	}

	public int getID() {
		return ID;
	}

	public void buyWater() {
		water += 2;
	}

	public void useWater() {
		water -= 1;
	}

	public long getWaterNo() {
		return water;
	}

	public void humanNewLocation(Coords coord) {
		System.out.println("MyID:" + ID);
		if (humanCoords.size() >= Config.NO_HUMANLOCATION_STORED) {
			humanCoords.poll();
		}
		humanCoords.add(coord);

		int minY = -8450;
		int maxY = 8315;
		System.out.println(coord.getY());

		if (firstLocation) {
			if (coord.getY() < (maxY + minY) / 2) {
				GUIMain.addTeamate(getID());
			}
			firstLocation = false;
		}
	}

	public Queue<Coords> getWholeHumanLocationQueue() {
		return humanCoords;
	}

	public void die() {
		// TODO Auto-generated method stub
		humanCoords.clear();
	}

	public void resetFirst() {
		// TODO Auto-generated method stub
		firstLocation = true;
	}

	public static Player getPlayerWithID(int playerIndex){// throws PlayerNotFoundException {
		for (Player p : Capturer.players) {
			if (p.getID() == playerIndex) {
				return p;
			}
		}
//		throw new PlayerNotFoundException();
		return null;

	}

	public static Player getPlayerWithIDString(String playerNoString) throws PlayerNotFoundException {
		int playerNo = 0;
		if (playerNoString.toUpperCase().equals("0A")) {
			playerNo = 10;
		} else {
			playerNo = Integer.parseInt(playerNoString);
		}
		
		// TODO Auto-generated method stub
		return getPlayerWithID(playerNo);
	}

	public void buyBoom() {
		// TODO Auto-generated method stub
		haveBoom=true;
		buyTime= new Date();
		playSound();
	}
	
	public void checkBombInvalid(){
		Date currentTime=new Date();
		long difference = currentTime.getTime() - buyTime.getTime(); 
		if(difference>60000){
			haveBoom=false;
		}
	}
	public String getBoomString(){
		
		if(haveBoom){
			return "(�z)";
		}else{
			return "";
		}
	}
	

	public static synchronized void playSound() {

		new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.

			private final int BUFFER_SIZE = 128000;
			private File soundFile;
			private AudioInputStream audioStream;
			private AudioFormat audioFormat;
			private SourceDataLine sourceLine;

			public void run() {
				String strFilename = "bomb.wav";
				try {
					soundFile = new File(strFilename);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}

				try {
					audioStream = AudioSystem.getAudioInputStream(soundFile);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}

				audioFormat = audioStream.getFormat();

				DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
				try {
					sourceLine = (SourceDataLine) AudioSystem.getLine(info);
					sourceLine.open(audioFormat);
				} catch (LineUnavailableException e) {
					e.printStackTrace();
					System.exit(1);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}

				sourceLine.start();

				int nBytesRead = 0;
				byte[] abData = new byte[BUFFER_SIZE];
				while (nBytesRead != -1) {
					try {
						nBytesRead = audioStream.read(abData, 0, abData.length);
					} catch (IOException e) {
						e.printStackTrace();
					}
					if (nBytesRead >= 0) {
						@SuppressWarnings("unused")
						int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
					}
				}

				sourceLine.drain();
				sourceLine.close();
			}

		}).start();
	}

}
