package utils;

public class ByteHexTool {
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static float hexStringToFloat(String myString){
		Long i = Long.parseLong(myString, 16);
        Float f = Float.intBitsToFloat(i.intValue());
//        System.out.println(f);
//        System.out.println(Integer.toHexString(Float.floatToIntBits(f)));
        return f;
	}
	
	
	public static String hexStringLittleEndianOrder(String inputString){
		String result="";
		for (int i=inputString.length()-2;i>=0;i-=2){
			result+=inputString.substring(i,i+2);
		}
		
		return result;
	}
	public static void main(String args[]){
		byte b1 = (byte) 'A';
		byte[] bArr={b1,b1};
		System.out.println(ByteHexTool.bytesToHex(bArr));
	}
	
	
}
