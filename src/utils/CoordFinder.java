/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author manheima3
 */
public class CoordFinder {

    public static int findYfromValue(int value, int maxValue, int maxY) {
        return (int)((double)maxY/maxValue*value);
    }
 public static int findXfromValue(int value, int maxValue, int maxX) {
        return (int)((double)maxX/maxValue*value);
    }
 

 public static double findYfromValue(double value, double maxValue, double maxY) {
     return (maxY/maxValue*value);
 }
public static double findXfromValue(double value, double maxValue, double maxX) {
     return (maxX/maxValue*value);
 }
}
