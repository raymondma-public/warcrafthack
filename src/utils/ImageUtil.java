package utils;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class ImageUtil {
	  public static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
	      BufferedImage dest = src.getSubimage(0, 0, rect.width, rect.height);
	      return dest; 
	   }
	  
	  public static BufferedImage cropImage(BufferedImage originalImage,int x, int y, int width, int height){
		// create a cropped image from the original image
		  BufferedImage croppedImage = originalImage.getSubimage(x, y, width, height);
		  return croppedImage;
	  }
}
