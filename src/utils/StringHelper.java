package utils;

import exception.StringNotFoundException;

public class StringHelper {
	public static String findAndGetFromHere(String wholeString,String findString) throws StringNotFoundException{
		String resultString = "";
		if ((wholeString.indexOf(findString) != -1)) {
			
			resultString = wholeString.substring((wholeString.indexOf(findString)));
		}else{
			throw new StringNotFoundException();
		}
		return resultString;
	}

}
