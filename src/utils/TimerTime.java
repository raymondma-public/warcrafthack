package utils;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import mainSys.GUIMain;

public class TimerTime {
//	private JLabel timeLabel;

	public TimerTime(int mSec,ActionListener aListener) {
//		timeLabel = new JLabel(new Date().toString());
//		add(timeLabel);

		Timer timer = new Timer(mSec, aListener);
		timer.setInitialDelay(1);
		timer.start();
	}



//	private static void createAndShowUI() {
//		JFrame frame = new JFrame("TimerTime");
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.add(new TimerTime());
//		frame.setLocationByPlatform(true);
//		frame.pack();
//		frame.setVisible(true);
//	}
//
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				createAndShowUI();
//			}
//		});
//	}
}